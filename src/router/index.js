import Vue from "vue";
import VueRouter from "vue-router";
import List from "@/components/list.vue";
import Cart from "@/components/cart.vue";
import Invoice from "@/components/invoice.vue";
import PageNotFound from "@/components/PageError.vue";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Product",
      component: List
    },
    {
      path: "/cart",
      name: "Shop",
      component: Cart
    },
    {
      path: "/invoice",
      name: "Invoice",
      component: Invoice
    },
    {
      path: "/404",
      component: PageNotFound
    },

    { path: "*", redirect: "/404" }
  ]
});

export default router;
