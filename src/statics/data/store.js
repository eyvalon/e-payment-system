import Vue from "vue";

export const store = Vue.observable({
  items: [
    {
      id: 1,
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSpRVRo82m8gZXR4f2gquUVENfjEKUp6Jlt0A&usqp=CAU",
      name: "Asparagus",
      price: "9.01",
      description: "fresh grocery",
      quantity: 0,
      maxQuantity: 10,
      inStock: "20"
    },
    {
      id: 2,
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS_aBkVVeK992cn5Wgg4OTRhhI4dK9NC_-R9Q&usqp=CAU",
      name: "Sauce",
      price: "7.82",
      description: "fresh grocery",
      quantity: 0,
      maxQuantity: 10,
      inStock: "20"
    },
    {
      id: 3,
      image:
        "https://importfood.com/media/zoo/images/mama-pork-main-large_528fd83d5e73593e68438ad1dbeb372c.jpg",
      name: "Instant Noodle",
      price: "3.22",
      description: "fresh grocery",
      quantity: 0,
      maxQuantity: 10,
      inStock: "20"
    },
    {
      id: 4,
      image:
        "https://imagesvc.meredithcorp.io/v3/mm/image?q=85&c=sc&poi=face&url=https%3A%2F%2Fimg1.cookinglight.timeinc.net%2Fsites%2Fdefault%2Ffiles%2Fstyles%2F4_3_horizontal_-_1200x900%2Fpublic%2Fimage%2F2016%2F02%2Fmain%2F1603p68-barilla-whole-grain-lasagna.jpg%3Fitok%3DoGCq0-Ms",
      name: "Lasgna",
      price: "8.33",
      description: "fresh grocery",
      quantity: 0,
      maxQuantity: 10,
      inStock: "20"
    },
    {
      id: 5,
      image:
        "https://cdn.shopify.com/s/files/1/0293/1231/9626/products/oi-ocha-green-tea-bags_2048x.jpg?v=1585111066",
      name: "Green Tea",
      price: "1.76",
      description: "fresh grocery",
      quantity: 0,
      maxQuantity: 10,
      inStock: "20"
    },
    {
      id: 6,
      image:
        "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcR_8w8XzlLMFS4DdEeuLj4tLksGevuTt3UNhw&usqp=CAU",
      name: "Promegranate",
      price: "1.85",
      description: "fresh grocery",
      quantity: 0,
      maxQuantity: 10,
      inStock: "20"
    }
  ],
  paymentOption: "",
  paid: false,
  toggleSidebar: false
});

export const mutations = {
  increaseQuantity(key) {
    let currentQuantity = store.items.find((item) => item.id === key).quantity;
    let currentMaxQuantity = store.items.find((item) => item.id === key)
      .maxQuantity;

    if (currentQuantity >= 0 && currentQuantity < currentMaxQuantity) {
      store.items.find((item) => item.id === key).quantity += 1;
    }
  },
  decreaseQuantity(key) {
    let currentQuantity = store.items.find((item) => item.id === key).quantity;

    if (currentQuantity > 0) {
      store.items.find((item) => item.id === key).quantity -= 1;
    }
  },
  addPaymentOptions(optionType) {
    store.paymentOption = optionType;
    // store.paid = true;
  },
  increaseQuantityAtIndex(index, value) {
    store.items.find((item) => item.id === index).quantity += value;
  },
  toggleSidebar() {
    return store.toggleSidebar;
  },
  TOGGLE_SIDEBAR() {
    store.toggleSidebar = !store.toggleSidebar;
  }
};
