### Description

Creating an responsive E-commerce product selling website, integrating payment system using PayPal or Credit Card.

## Images

| Sample pictures             | Sample pictures             |
| --------------------------- | --------------------------- |
| ![Alt Text](./images/1.png) | ![Alt Text](./images/2.png) |
| ![Alt Text](./images/3.png) | ![Alt Text](./images/4.png) |

### LIST OF IMPLEMENTATIONS

- [x] Implmenting use of shared variables from main page to cart, payment and invoice
- [x] Visualising buttons with CSS styling
- [x] Formatting and validating credentials for cart page (currently disabed for security purposes)

### LIST OF IMPROVEMENTS:

- Responsive templating from web to mobile (apart from Home)
- Integrating Paypal system

### Note:

For better integration, API system should be requested and stored in replacement of the current array that is used for mockup.

---

## Project setup

```
npm install
```

## Running project

To run on default port 8080:

```
npm run serve
```

To run on a certain port (i.e. 3000):

```
npm run serve -- --port 3000
```
